const webpack = require('webpack')

const commonPaths = require('./paths')

module.exports = {
  mode: 'development',
  output: {
    filename: '[name].js',
    path: commonPaths.outputPath,
    chunkFilename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.(css|sass|scss)$/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader'
        ]
      }
    ]
  },
  resolve: {
    alias: {
      'react-dom': '@hot-loader/react-dom'
    }
  },
  devServer: {
    port: 3000,
    contentBase: commonPaths.outputPath,
    historyApiFallback: true,
    compress: true,
    hot: true
  },
  plugins: [new webpack.HotModuleReplacementPlugin()]
}
