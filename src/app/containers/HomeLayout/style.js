import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    overflow: 'scroll',
    paddingTop: 112
  }
})