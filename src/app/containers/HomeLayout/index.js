import React  from 'react'
import { View } from 'react-native'
import { StatusBar } from 'expo-status-bar'

import { Header } from '@common/components'
import { Feed } from '@common/containers'

import styles from './style'

const HomeLayout = () => {
  return (
    <View style={ styles.container }>
      <StatusBar style="dark"/>
      <Header/>
      <Feed/>
    </View>
  )
}

export default HomeLayout