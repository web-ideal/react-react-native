import React from 'react'

import { HomeLayout } from '@app/containers'

const App = () => {
  return (
    <HomeLayout/>
  )
}

export default App