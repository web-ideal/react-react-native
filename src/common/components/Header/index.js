import React  from 'react'

import Header from './Header'

import logoPic from '@assets/pictures/logo_wb.jpg'
import facebookIcon from '@assets/icons/facebook.png'
import twitterIcon from '@assets/icons/twitter.png'
import userIcon from '@assets/icons/user.png'

export default () => (
  <Header logoPic={ logoPic } facebookIcon={ facebookIcon } twitterIcon={ twitterIcon } userIcon={ userIcon }/>
)