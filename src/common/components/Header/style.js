import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  Header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    paddingTop: 20,
    backgroundColor: '#FFF'
  },
  wrapper: {
    height: 64,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: '#D04D47',
    borderBottomWidth: 1,
    padding: 8
  },
  wrapperLeft: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  logo: {
    width: '50%',
    height: 48,
    resizeMode: 'contain'
  },
  wrapperRight: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  socials: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
    marginLeft: 8
  },
  user: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    marginLeft: 16
  },
  menu: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    padding: 8
  },
  menuItem: {
    fontSize: 12
  }
})