import React  from 'react'

import './style.sass'

const Header = ({ logoPic, facebookIcon, twitterIcon, userIcon }) => (
  <div className='Header'>
    <div className="wrapper">
      <div className="left">
        <div id="logo">
          <img src={ logoPic } alt="Logo"/>
        </div>
      </div>
      <div className="right">
        <div className="icon">
          <img src={ facebookIcon } alt="Facebook"/>
        </div>
        <div className="icon">
          <img src={ twitterIcon } alt="Twitter"/>
        </div>
        <div className="user">
          <img src={ userIcon } alt="User"/>
        </div>
      </div>
    </div>
    <div className="menu">
      <div className="item">
        <p>PROTEST</p>
      </div>
      <div className="item">
        <p>INFLUENCE</p>
      </div>
      <div className="item">
        <p>NEW THINGS</p>
      </div>
      <div className="item">
        <p>POLLS AND QUIZ</p>
      </div>
    </div>
  </div>
)

export default Header