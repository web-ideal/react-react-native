import React  from 'react'
import { Image, Text, TouchableOpacity, View }  from 'react-native'

import styles from './style'

const Header = ({ logoPic, facebookIcon, twitterIcon, userIcon }) => (
  <View style={ styles.Header }>
    <View style={ styles.wrapper }>
      <View style={ styles.wrapperLeft }>
        <Image
          style={ styles.logo }
          source={ logoPic }
        />
      </View>
      <View style={ styles.wrapperRight }>
        <Image
          style={ styles.socials }
          source={ facebookIcon }
        />
        <Image
          style={ styles.socials }
          source={ twitterIcon }
        />
        <Image
          style={ styles.user }
          source={ userIcon }
        />
      </View>
    </View>
    <View style={ styles.menu }>
      <TouchableOpacity>
        <Text style={ styles.menuItem }>PROTEST</Text>
      </TouchableOpacity>
      <TouchableOpacity>
        <Text style={ styles.menuItem }>INFLUENCE</Text>
      </TouchableOpacity>
      <TouchableOpacity>
        <Text style={ styles.menuItem }>NEW THINGS</Text>
      </TouchableOpacity>
      <TouchableOpacity>
        <Text style={ styles.menuItem }>POLLS AND QUIZ</Text>
      </TouchableOpacity>
    </View>
  </View>
)

export default Header