import React  from 'react'
import { View, Text, Image }  from 'react-native'

import styles from './style.js'

const Post = ({ data: { date, title, picture, author, content, score, votes, views } }) => (
  <View style={ styles.Post }>
    <View style={ styles.date }>
      <Text style={ styles.dateText }>{ `Posted ${ date }` }</Text>
    </View>
    <View>
      <Text style={ styles.title }>{ title }</Text>
      <Image
        style={ styles.picture }
        source={ picture }
      />
      <Text style={ styles.author }>{ author }</Text>
      <Text style={ styles.text }>{ content }</Text>
    </View>
    <View style={ styles.info }>
      <Text style={ styles.infoItem }>
        { "Score " }
        <Text style={ styles.infoItemHL }>{ score }</Text>
      </Text>
      <Text style={ styles.infoItem }>
        { "Votes " }
        <Text style={ styles.infoItemHL }>{ votes }</Text>
      </Text>
      <Text style={ styles.infoItem }>
        { "Views " }
        <Text style={ styles.infoItemHL }>{ views }</Text>
      </Text>
    </View>
  </View>
)

export default Post