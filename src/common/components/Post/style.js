import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  Post: {
    paddingTop: 16,
    marginBottom: 32,
    backgroundColor: '#F2F3F5'
  },
  date: {
    paddingHorizontal: 8
  },
  dateText: {
    color: '#5088C2'
  },
  title: {
    marginVertical: 8,
    paddingHorizontal: 8,
    fontSize: 20,
    fontWeight: 'bold'
  },
  picture: {
    width: '100%',
    height: 220,
    resizeMode: 'cover'
  },
  author: {
    marginVertical: 8,
    paddingHorizontal: 8,
    fontWeight: 'bold',
    textDecorationLine: 'underline',
    color: '#808080'
  },
  text: {
    marginBottom: 8,
    paddingHorizontal: 8,
    fontSize: 16
  },
  info: {
    alignItems: 'center',
    flexDirection: 'row',
    width: '100%',
    paddingVertical: 16,
    paddingHorizontal: 8,
    backgroundColor: '#000000'
  },
  infoItem: {
    marginRight: 16,
    color: '#FFFFFF'
  },
  infoItemHL: {
    fontWeight: 'bold',
    color: '#4D86C1'
  }
})