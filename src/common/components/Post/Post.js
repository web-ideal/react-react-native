import React  from 'react'

import './style.sass'

const Post = ({ data: { date, title, picture, author, content, score, votes, views } }) => (
  <div className="Post">
    <div className="date">
      <p>{ `Posted ${ date }` }</p>
    </div>
    <div className="content">
      <h2>{ title }</h2>
      <div className="picture">
        <img src={ picture }/>
      </div>
      <p className="author">{ author }</p>
      <h3>{ content }</h3>
    </div>
    <div className="info">
      <div className="item">
        <p>Score <span>{ score }</span></p>
      </div>
      <div className="item">
        <p>Votes <span>{ votes }</span></p>
      </div>
      <div className="item">
        <p>Views <span>{ views }</span></p>
      </div>
    </div>
  </div>
)

export default Post