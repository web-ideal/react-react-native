import React  from 'react'

import Post from './Post'

export default (props) => (
  <Post { ...props }/>
)