import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  Feed: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  bar: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
    paddingHorizontal: 32,
    backgroundColor: '#000000'
  },
  barLabel: {
    color: '#FFFFFF'
  }
})