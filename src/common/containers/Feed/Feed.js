import React  from 'react'

import './style.sass'

const Feed = ({ children }) => (
  <div className='Feed'>
    <div className="bar">
      <h5>PLATFORM FOR UNHEARD NEWS</h5>
    </div>
    <div className="list">
      { children }
    </div>
  </div>
)

export default Feed