import React  from 'react'
import { View, Text, ScrollView }  from 'react-native'

import styles from './style.js'

const Feed = ({ children }) => (
  <View style={ styles.Feed }>
    <View style={ styles.bar }>
      <Text style={ styles.barLabel }>PLATFORM FOR UNHEARD NEWS</Text>
    </View>
    <ScrollView>
      { children }
    </ScrollView>
  </View>
)

export default Feed