import React, { useState } from 'react'

import Feed from './Feed'
import { Post } from '@common/components'

import post1Pic from '@assets/pictures/post1.jpg'
import post2Pic from '@assets/pictures/post2.jpg'
import post3Pic from '@assets/pictures/post3.jpg'

const fakePosts = [
  {
    id: 0,
    date: '10 seconds ago',
    title: "This random knowledge quiz may be difficult.",
    picture: post1Pic,
    author: "quizly.co",
    content: "This Random Knowledge Quiz May Be Difficult, But You Should Try To Pass It Anyway. And some more text here...",
    score: 143,
    votes: 75,
    views: 2147
  },
  {
    id: 1,
    date: '1 minute ago',
    title: "How to know if your date is a keeper",
    picture: post2Pic,
    author: "dailyarmy.com",
    content: "How to know if your date is a keeper... and lorem ipsum then...",
    score: 125,
    votes: 72,
    views: 2719
  },
  {
    id: 2,
    date: '3 minutes ago',
    title: "Trumpworld Fears Its ‘Nightmare Scenario’ Is Coming True",
    picture: post3Pic,
    author: "www.thedailybeast.com",
    content: "Trumpworld Fears Its ‘Nightmare Scenario’ Is Coming True. Also...",
    score: 124,
    votes: 58,
    views: 1469
  }
]

export default () => {
  const [posts] = useState(fakePosts)

  return (
    <Feed>
      {
        posts.map((item) => (
          <Post key={ item.id } data={ item }/>
        ))
      }
    </Feed>
  )
}