import React  from 'react'

import { Header } from '@common/components'
import { Feed } from '@common/containers'

import './style.sass'

const HomeLayout = () => {
  return (
    <div className='HomeLayout'>
      <Header/>
      <Feed/>
    </div>
  )
}

export default HomeLayout