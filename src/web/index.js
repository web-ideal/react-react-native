import React from 'react'
import { hot } from 'react-hot-loader/root'

import { HomeLayout } from '@web/containers'

import './global.sass'

const App = () => {
  return (
    <HomeLayout/>
  )
}

export default hot(App)
